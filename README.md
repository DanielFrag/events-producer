# events-producer

## Sinopse
Aplicação criada para enviar, de forma bem simples, eventos para o kafka

## Formato da mensagem
Toda mensagem será um json contendo o nome do evento e um timestamp gerado aleatoriamente
```json
{
  "event": "sunda",
  "timestamp": "2019-10-01T00:00:00Z"
}
```

## Variáveis de ambiente
- BROKER_LIST
  - Obrigatória
  - Deve conter a lista de \<host\>:\<porta\> dos brokers do kafka separados por vírgula
  - Deve conter ao menos um item
- EVENTS_LIST
  - Opcional
  - Lista com os nomes dos eventos separados por vírgula
- NUMBER_OF_MESSAGES
  - Opcional
  - Número de mensagens a serem enviadas para o kafka
## Build
```
$ go build
```
## Run
```
$ export BROKER_LIST=kafka:9092 EVENTS_LIST=bobra,acaxike,sunda NUMBER_OF_MESSAGES=15 && ./events-producer
```