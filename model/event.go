package model

import "time"

// Event struct representing a single event
type Event struct {
	Name      string    `json:"event"`
	Timestamp time.Time `json:"timestamp"`
}
