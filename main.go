package main

import (
	"bytes"
	"encoding/json"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"time"

	"gitlab.com/DanielFrag/events-producer/model"
	"gitlab.com/DanielFrag/events-producer/producer"
)

func main() {
	eventsList := os.Getenv("EVENTS_LIST")
	if eventsList == "" {
		eventsList = "foo,bar,sunda"
	}
	var numberOfMessages = os.Getenv("NUMBER_OF_MESSAGES")
	if numberOfMessages == "" {
		numberOfMessages = "10"
	}
	eventNames := strings.Split(eventsList, ",")
	nMessages, strconvError := strconv.Atoi(numberOfMessages)
	if strconvError != nil {
		panic(strconvError)
	}
	events := make([]model.Event, nMessages)
	for i := 0; i < nMessages; i++ {
		index := rand.Int() % len(eventNames)
		if index < 0 {
			index *= -1
		}
		events[i] = model.Event{
			Name:      eventNames[index],
			Timestamp: time.Date(2019, 11, rand.Int()%28, rand.Int()%24, 0, 0, 0, time.UTC),
		}
	}
	kafkaProducer := &producer.KafkaProducer{}
	kafkaBrokerList := os.Getenv("BROKER_LIST")
	if kafkaBrokerList == "" {
		panic("broker list is undefined")
	}
	options := make(map[string]interface{})
	options["bootstrap.servers"] = kafkaBrokerList
	kafkaProducer.StartProducer(options)
	for _, event := range events {
		b := new(bytes.Buffer)
		json.NewEncoder(b).Encode(event)
		kafkaProducer.PostEvent(b.Bytes())
	}
	kafkaProducer.StopProducer()
}
