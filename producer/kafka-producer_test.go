package producer

import (
	"fmt"
	"os"
	"testing"
	"time"

	"gopkg.in/confluentinc/confluent-kafka-go.v1/kafka"
)

var testTopicName string
var kafkaBrokerList string
var options map[string]interface{}
var consumer *kafka.Consumer

func configConsumer() error {
	var newConsumerError error
	consumer, newConsumerError = kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": kafkaBrokerList,
		"group.id":          "testGroup",
		"auto.offset.reset": "earliest",
	})
	if newConsumerError != nil {
		return newConsumerError
	}
	consumer.Subscribe(testTopicName, nil)
	return nil
}
func TestKafkaProducer(t *testing.T) {
	producer := &KafkaProducer{}
	now := time.Now()
	originalMsg := fmt.Sprintf("test now: %s", now.String())
	producer.StartProducer(options)
	producer.PostEvent([]byte(originalMsg))
	timeout, _ := time.ParseDuration("10s")
	message, consumerError := consumer.ReadMessage(timeout)
	if consumerError != nil {
		t.Error(consumerError)
		return
	}
	msgStr := string(message.Value)
	if msgStr != originalMsg {
		t.Errorf("Invalid message. Actual: %s / Expected: %s", msgStr, originalMsg)
	}
}

func TestMain(m *testing.M) {
	testTopicName = "events_test"
	kafkaBrokerList = "kafka:9092"
	os.Setenv("TOPIC_NAME", testTopicName)
	options = make(map[string]interface{})
	options["bootstrap.servers"] = kafkaBrokerList
	configConsumer()
	exitCode := m.Run()
	os.Exit(exitCode)
}
